package eu.hautil;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

class DeviseTest {

    @Test
    void add() throws MonnaieDifferenteException {
        Devise m12CHF = new Devise(12, "CHF");
        Devise m14CHF = new Devise(14, "CHF");
        Devise expected = new Devise(26, "CHF");
        Devise result = m12CHF.add(m14CHF);
        Assertions.assertEquals(expected, result, "Erreur lors de l'addition ");
        System.out.println(result.toString());



    }

    @Test
    void TestEquals() {
        Devise m12CHF= new Devise(12, "CHF");
        Devise m14CHF= new Devise(14, "CHF");
        Devise m14USD = new Devise(14, "USD");
        // tester que m12CHF est bien égale à m12CHF
        // tester que m12CHF n'est pas égale à m14CHF
        // tester que m14CHF n'est pas égale à m14USD

        Assertions.assertEquals(m12CHF, m12CHF, "Erreur : m12CHF n'est pas egale a m12CHF");
        System.out.println("m12CHF est bien egale a m12CHF");

        Assertions.assertNotEquals(m12CHF,m14CHF,"Erreur : m12CHF est egale a m14CHF");
        System.out.println("m12CHF est bien different de m14CHF");

        Assertions.assertNotEquals(m14CHF,m14USD,"Erreur : m14CHF est egale a m14USD");
        System.out.println("m14CHF est bien different de m14USD");


    }

    @BeforeEach
    void testavant() {
        System.out.println("Im Before");
    }

    @AfterEach
    void testapres() {
        System.out.println("Im After");
    }
}
