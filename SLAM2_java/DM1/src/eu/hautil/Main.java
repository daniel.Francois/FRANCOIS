package eu.hautil;

import java.sql.*;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Fonction accesRole = new Fonction();

        try {

            System.out.println("Bienvenue dans le Main.");

            Scanner sc = new Scanner(System.in);
            int choice = 1;

            while(choice>=1 && choice<=5){
                System.out.print("\n");
                System.out.println("Choisissez une option");
                System.out.println("1: Visualiser le contenu la table \n" +
                        "2: Ajouter un élément à la table \n" +
                        "3: Supprimer un élément de la table \n" +
                        "4: Sélectionner un rôle en fonction de l'ID \n" +
                        "5: Entrez votre identifiant et votre mdp"+
                        "6: Quitter");
                System.out.print("Tapez le chiffre correspondant à votre choix: ");
                choice = sc.nextInt();

                switch(choice) {

                    case 1 -> accesRole.afficherRoles();


                    case 2 -> {
                        System.out.println("Renseignez l'ID : ");
                        int id = sc.nextInt();
                        sc.nextLine(); //C'est pour "aspirer" le bouton entrée qui est compté comme une valeur

                        System.out.println("Renseignez le rôle : ");
                        String libelle = sc.nextLine();

                        accesRole.ajouterRoles(id, libelle);
                    }

                    case 3 -> {
                        System.out.print(" rentrer l'ID du rôle que vous souhaitez supprimer : ");
                        int id2 = sc.nextInt();
                        accesRole.supprimerRoles(id2);

                    }


                    case 4 -> {
                        System.out.println("rentrer l'ID du rôle que vous souhaitez sélectionner : \n");
                        int id3 = sc.nextInt();
                        sc.nextLine();
                        accesRole.afficherRoleById(id3);
                    }

                    case 5 -> {

                        System.out.println(" Bonjour"+ login);
                    }
                }
            }


        } catch (SQLException e){
            e.printStackTrace();
        }
    }
}
