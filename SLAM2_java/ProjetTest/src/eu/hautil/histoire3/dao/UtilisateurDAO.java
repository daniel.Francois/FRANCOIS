package eu.hautil.histoire3.dao;

import eu.hautil.histoire3.model.Role;
import eu.hautil.histoire3.model.Utilisateur;

import java.sql.*;
import java.util.ArrayList;

public class UtilisateurDAO {

    String driver = "com.mysql.cj.jdbc.Driver"; //
    String urlBDD = "jdbc:mysql://localhost:3306/who"; //pour la connexion de BDD
    String loginBDD = "root";//identifiant
    String passwdBDD = "root";//mot de passe

    String requete5 = "SELECT u.id, u.login, u.mdp, u.role, u.date_creation, u.nom, u.prenom, r.id_role, r.libelle FROM who_utilisateurs u INNER JOIN who_roles r ON u.role = r.id_role";
    String requete6 = "SELECT u.id, u.login, u.mdp, u.role, u.date_creation, u.nom, u.prenom, r.id_role, r.libelle FROM who_utilisateurs u INNER JOIN who_roles r ON u.role = r.id_role WHERE u.login = ?";
    String requete7 = "INSERT INTO  who_utilisateurs(login, mdp, role, date_creation, nom, prenom) VALUES (?,PASSWORD(?),?,?,?,?)";
    String requete8 = "DELETE FROM who_utilisateurs WHERE login = ?";


    public UtilisateurDAO() throws SQLException {
    }

    private Connection getConnexion() throws SQLException, ClassNotFoundException {
        Class.forName(driver);
        Connection conn = DriverManager.getConnection(urlBDD, loginBDD, passwdBDD);
        System.out.println("Connexion OK \n");

        return conn;

    }

    public ArrayList<Utilisateur> getALLUtilisateurs() throws SQLException {
        Connection conn = DriverManager.getConnection(urlBDD, loginBDD, passwdBDD);
        Statement stmt = conn.createStatement();
        ResultSet firstTable = stmt.executeQuery(requete5);
        System.out.println("\n");
        System.out.println("Voici la table  : ");

        while (firstTable.next()) {
            System.out.print("Id : " + firstTable.getString(1) + " | ");
            System.out.print("Login : " + firstTable.getString(2) + " | ");
            System.out.print("Mdp : " + firstTable.getString(3) + " | ");
            System.out.print("Rôle : " + firstTable.getString(4) + " | ");
            System.out.print("Date Création : " + firstTable.getString(5) + " | ");
            System.out.print("Nom : " + firstTable.getString(6) + " | ");
            System.out.print("Prénom : " + firstTable.getString(7) + "\n");

        }
        return null;
    }



    public Utilisateur getUtilisateurByLogin(String login) throws SQLException {

        Connection conn = DriverManager.getConnection(urlBDD, loginBDD, passwdBDD);
        PreparedStatement pstmt2 = conn.prepareStatement(requete6);
        pstmt2.setString(1, login);

        ResultSet res1 = pstmt2.executeQuery();

        while (res1.next()) {
            System.out.print("Id : " + res1.getString(1) + " | ");
            System.out.print("Login : " + res1.getString(2) + " | ");
            System.out.print("Mdp : " + res1.getString(3) + " | ");
            System.out.print("Rôle : " + res1.getString(4) + " | ");
            System.out.print("Date Création : " + res1.getString(5) + " | ");
            System.out.print("Nom : " + res1.getString(6) + " | ");
            System.out.print("Prénom : " + res1.getString(7) + "");
            System.out.print("Rôle ID : " + res1.getString(8) + " ");
            System.out.print("Libellé Rôle : " + res1.getString(9) + "\n");

        }
        res1.close();
        return null;
    }
    public void ajouterUtilisateur(Utilisateur u) throws SQLException {


        Connection conn = DriverManager.getConnection(urlBDD, loginBDD, passwdBDD);
        Statement stmt = conn.createStatement();
        PreparedStatement pstmt = conn.prepareStatement(requete7);

        pstmt.setString(1, u.getLogin());
        pstmt.setString(2, u.getMdp());
        setRole(3, u.getRole());
        pstmt.setString(4, u.getDate_creation());
        pstmt.setString(5, u.getNom());
        pstmt.setString(6, u.getPrenom());

        int res2 = pstmt.executeUpdate();
        System.out.println("Vos données ont bien été inserées :  " + res2 + "\n");
    }

    private void setRole(int i, Role role) {
    }

    public void SupprimerUtilisateur(String login) throws SQLException{
        Connection conn = DriverManager.getConnection(urlBDD, loginBDD, passwdBDD);
        Statement stmt = conn.createStatement();
        PreparedStatement pstmt = conn.prepareStatement(requete8);
        pstmt.setString(1, login);

        int res3 = pstmt.executeUpdate();

        System.out.println(" la suppression des données : " + res3 + "\n");

    }








}
