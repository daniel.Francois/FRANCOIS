package eu.hautil.histoire3.dao;

import eu.hautil.histoire3.model.Role;






import java.sql.*;
import java.util.ArrayList;

public class RoleDAO {

    String driver = "com.mysql.cj.jdbc.Driver"; //
    String urlBDD = "jdbc:mysql://localhost:3306/who"; //pour la connexion de BDD
    String login = "root"; //identifiant
    String passwd = "root"; //mot de passe

    String requete1 = "SELECT id_role, libelle FROM who_roles"; // Initialisation de la requête
    String requete2 = "SELECT libelle FROM who_roles WHERE id_role = ?";
    String requete3 = "INSERT INTO who_roles VALUES (?,?)";
    String requete4 = "DELETE FROM who_roles WHERE id_role = ?";

    public RoleDAO() throws SQLException {
    }

    private Connection getConnexion() throws SQLException, ClassNotFoundException { // Se connecte à la BDD
        Class.forName(driver);
        Connection conn = DriverManager.getConnection(urlBDD, login, passwd);
        System.out.println("Connexion OK \n");

        return conn;
    }

    public ArrayList<Role> getALLRoles() throws SQLException { // Créer une liste de tous les rôles


        Connection conn = DriverManager.getConnection(urlBDD, login, passwd); // Se connecte à la BDD
        Statement stmt = conn.createStatement(); //
        ResultSet firstTable = stmt.executeQuery(requete1); //Récupère la table role après le INSERT
        System.out.println("Voici la table rôle  : ");
        while (firstTable.next()) {
            System.out.print("Id : " + firstTable.getString(1) + " | ");
            System.out.print("Libelle : " + firstTable.getString(2) + " \n ");
        }

        return null;
    }

    public Role getRoleById(int id) throws SQLException {

        Connection conn = DriverManager.getConnection(urlBDD, login, passwd);
        //Statement stmt = conn.createStatement();
        PreparedStatement pstmt3 = conn.prepareStatement(requete2);
        pstmt3.setInt(1, id);

        ResultSet res2 = pstmt3.executeQuery();
        while (res2.next()) {
            System.out.print("Id : " + res2.getString(1));
        }
        res2.close();
        return null;

    }

    public void ajouterRole(Role r) throws SQLException {

        Connection conn = DriverManager.getConnection(urlBDD, login, passwd);
        Statement stmt = conn.createStatement();
        PreparedStatement pstmt = conn.prepareStatement(requete3);

        pstmt.setInt(1, r.getId());
        pstmt.setString(2, r.getLibelle());

        int res3 = pstmt.executeUpdate(); //INSERT
        System.out.println(" données ont bien été inserées :  " + res3 + "\n");
    }


    public void SupprimerRole(int id) throws SQLException {


        Connection conn = DriverManager.getConnection(urlBDD, login, passwd);
        Statement stmt = conn.createStatement();
        PreparedStatement pstmt2 = conn.prepareStatement(requete4);
        pstmt2.setInt(1, id);

        int res4 = pstmt2.executeUpdate();
        System.out.println(res4 + " : Donc on a  bien supprime");
    }

}