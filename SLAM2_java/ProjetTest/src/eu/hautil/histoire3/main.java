package eu.hautil.histoire3;


import eu.hautil.histoire3.dao.RoleDAO;


import eu.hautil.histoire3.dao.UtilisateurDAO;
import eu.hautil.histoire3.model.Role;
import eu.hautil.histoire3.model.Utilisateur;


import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class Main {

    public static void main(String[]args) throws SQLException {
        RoleDAO myRole = new RoleDAO();
        UtilisateurDAO myUtilisateur = new UtilisateurDAO();





        try {

            System.out.println("\n" + "Bienvenue dans l'application Main.java !");

            Scanner sc = new Scanner(System.in);
            int choice = 1;

            while(choice>=1 && choice<=8) {
                System.out.print("\n");
                System.out.println("vous pouvez choisir maintenant ");
                System.out.println("""
                        1 - Pour regarder le contenu la table\s
                        2 - Afficher un rôle en fonction de l'ID\s
                        3 - Ajouter un rôle\s
                        4 - Supprimer un rôle\s
                        5 - Pour regarder la table utilisateurs\s
                        6 - Afficher un utilisateur par le login\s
                        7 - Ajouter un utilisateur\s
                        8 - Supprimer un utilisateur\s
                        9 - Quitter\s
                        """);
                System.out.print("Votre choix : ");
                choice = sc.nextInt();
                sc.nextLine();



                String login;
                String mdp;
                Role role ;
                String date_creation = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
                String nom;
                String prenom;

                switch (choice) {

                    case 1 -> myRole.getALLRoles();


                    case 2 -> {
                        System.out.print("cliquez l'ID du rôle que vous souhaitez visualiser : ");
                        int id = sc.nextInt();
                        sc.nextLine();
                        Role resultat = myRole.getRoleById(id);
                        System.out.print(resultat);
                        System.out.print("\n");


                    }

                    case 3 -> {
                        System.out.print("Renseignez l'ID : ");
                        int id = sc.nextInt();
                        sc.nextLine(); //C'est pour "aspirer" le bouton entrée qui est compté comme une valeur

                        System.out.print("Renseignez le rôle : ");
                        String libelle = sc.nextLine();

                        Role r = new Role(id, libelle);
                        myRole.ajouterRole(r);

                    }


                    case 4 -> {
                        System.out.print("Veuillez rentrer l'ID du rôle que vous souhaitez supprimer : ");
                        int id = sc.nextInt();
                        sc.nextLine();
                        myRole.SupprimerRole(id);

                    }

                    case 5 -> {

                        myUtilisateur.getALLUtilisateurs();

                    }

                    case 6 -> {
                        System.out.print("Rentrez le login de l'utilisateur que vous souhaitez afficher : ");
                        login = sc.nextLine();
                        myUtilisateur.getUtilisateurByLogin(login);

                    }


                    case 7 -> {
                        System.out.print("Rentrez le login : ");
                        login = sc.nextLine();

                        System.out.print("Rentrez mot de passe : ");
                        mdp = sc.nextLine();

                        System.out.print("Rentrez le rôle (1|2|3|4:)");
                        role = sc.nextRole();
                        sc.nextLine();

                        System.out.print("Rentrez le nom : ");
                        nom = sc.nextLine();

                        System.out.print("Rentrez le prénom : ");
                        prenom = sc.nextLine();

                        Utilisateur u= new Utilisateur(login, mdp,role, date_creation, nom, prenom);
                        myUtilisateur.ajouterUtilisateur(u);

                    }

                    case 8 -> {
                        System.out.print("Rentrez lOGIN de l'utilisateur que vous souahitez supprimer : ");
                        login = sc.nextLine();
                        myUtilisateur.SupprimerUtilisateur(login);
                    }

















                    default -> System.out.println("okkkkkkkkkkkk reviens plus ici stp");

                }
            }


        } catch (SQLException e){
            e.printStackTrace();
        }
    }


}
