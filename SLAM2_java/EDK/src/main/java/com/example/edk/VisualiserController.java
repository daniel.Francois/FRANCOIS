package com.example.edk;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class VisualiserController implements Initializable {




    @FXML
    private TableView<Client> VisualTable;

    @FXML
    private TableColumn<Client, Integer> client_id;

    @FXML
    private TableColumn<Client, String> VisualiserNom;

    @FXML
    private TableColumn<Client, String> VisualiserPrenom;

    @FXML
    private TableColumn<Client, String> VisualiserAdresse;

    @FXML
    private TableColumn<Client, String> VisualiserVille;

    @FXML
    private TableColumn<Client,String > VisualiserVille1;




    Stage dialogStage = new Stage();
    Scene scene;


    Connection connection = null;
    PreparedStatement preparedStatement = null;
    ResultSet rs  = null;

    int index = -1;

    ObservableList<Client> listM;
    private Stage stage;


    public VisualiserController() {
        connection = ConnectionUtil.connectdb();
    }


    @FXML

    public void HandleVisualiserRetour(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        dialogStage = (Stage) source.getScene().getWindow();
        dialogStage.close();
        scene = new Scene(FXMLLoader.load(getClass().getResource("Accueil.fxml")));
        dialogStage.setScene(scene);

        dialogStage.show();
    }

    public void handleupdate() {
        client_id.setCellValueFactory(new PropertyValueFactory<Client, Integer>("id"));
        VisualiserNom.setCellValueFactory(new PropertyValueFactory<Client, String>("nom"));
        VisualiserPrenom.setCellValueFactory(new PropertyValueFactory<Client, String>("prenom"));
        VisualiserAdresse.setCellValueFactory(new PropertyValueFactory<Client, String>("adresse"));
        VisualiserVille.setCellValueFactory(new PropertyValueFactory<Client, String>("ville"));
        VisualiserVille1.setCellValueFactory(new PropertyValueFactory<Client, String>("accessoire"));

        listM = new ConnectionUtil().getDataClient();
        VisualTable.setItems(listM);
    }





        public void HandleVisualiserButton(ActionEvent event) throws IOException {


        String sql = "select * from client ";





            Node source = (Node) event.getSource();
            dialogStage = (Stage) source.getScene().getWindow();
            dialogStage.close();
            scene = new Scene(FXMLLoader.load(getClass().getResource("Clientsupr.fxml")));
            dialogStage.setScene(scene);
            dialogStage.show();

        
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        handleupdate();

    }
}
