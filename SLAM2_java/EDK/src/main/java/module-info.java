module com.example.edk {
    requires javafx.controls;
    requires javafx.fxml;
    requires  javafx.graphics;
    requires java.sql;
    requires java.desktop;


    opens com.example.edk to javafx.fxml;
    exports com.example.edk;
}