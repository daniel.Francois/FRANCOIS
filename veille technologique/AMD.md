
AMD a annoncé avoir corrigé de nombreuses failles de sécurité dans ses pilotes graphiques pour Windows 10 .
Des failles corrigées en avril
La correction de ces vulnérabilités a été graduelle, et les dernières d'entre elles ont été corrigées en avril, lors de la sortie de la version 21.4.1 de la suite logicielle d'AMD, les Radeon Software Adrenalin. 
    Des vulnérabilités importantes
Sur son site, AMD a annoncé avoir corrigé plusieurs failles de sécurité qui plombaient ses pilotes graphiques sur Windows 10, nécessaires pour tous les possesseurs de cartes graphiques de la marque. Parmi les vulnérabilités les plus sévères corrigées, certaines permettaient de l'escalation de privilèges, de l'exécution de code non autorisée, du déni de service, de la divulgation d'informations ou encore de déposer un fichier DLL malveillant à certains emplacements. Toujours la semaine dernière, d'importants correctifs de sécurité ont également été déployés par l'entreprise pour corriger des vulnérabilités présentes dans ses processeurs pour serveurs AMD

source: google alerts

lien: https://www.clubic.com/pro/entreprise/amd/actualite-393504-amd-une-douzaine-d-exploits-de-securite-dans-ses-pilotes-pour-windows-enfin-corriges.html
