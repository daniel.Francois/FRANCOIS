@extends('layouts.app')
@section('content')

<div class="container">
<form method="post" action="{{ route('hara.store')}}" >
@csrf


  <div class="form-group" >
    <label for="nom">Nom de cheval </label>
    <input type="text" class="form-control" name=nom id="nom" placeholder="Nom" required>
  </div>

  <div class="form-group">
    <label for="age">Son Age</label>
    <input type="number" class="form-control" name=age  id="age" value="0" min="0" max="80" required>
  </div>


  <div class="form-group">
    <label for="nom">Couleur principal </label>
    <input type="text" class="form-control" name=couleur id="nom" placeholder="Nom" required>
  </div>


  <div class="form-group">
    <label for="nom">le Nom de propriétaire  </label>
    <input type="text" class="form-control" name=proprieteNom id="nom" placeholder="Nom" required>
  </div>


  <div class="form-group">
    <label for="nom"> le Prenom de propriétaire </label>
    <input type="text" class="form-control" name=proprietePrenom id="nom" placeholder="Nom" required>
  </div>



  <div class="form-check">
    <input type="checkbox" class="form-check-input" id="exampleCheck1">
    <label class="form-check-label" for="exampleCheck1">Check me out</label>
  </div>


  <button type="submit" class="btn btn-primary">Submit</button>
</form>
</div>
@stop