<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\Reservation; 

class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('createReservation');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {  
        $validatedData = $request->validate([
            'dateDebut' => 'required|date|before:tomorrow',
            'dateFin' => 'required|date|after:datef',
            'idperiode'=> 'required|between:1,3'
      ]);






        // dd($request);
        $reservation=new Reservation();
        $dated=$request->input('dateDebut');
        $datef=$request->input('dateFin');
        $idperiode=$request->input('idperiode');
        $reservation->dateD=$dated;
        $reservation->dateF=$datef;
        $reservation->idPeriode=$idperiode;
        $reservation->save();
        return redirect()->back();
     
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}