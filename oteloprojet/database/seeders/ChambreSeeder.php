<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Chambre;

class ChambreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        for ($t=0; $t <=30; $t++) {

            $tbl = array('A', 'B', 'C');
            $i = rand(0,2);
        
            $chambre = Chambre::create([
                'nbCouchage' => rand(1,4),
                'porte' => $tbl[$i],
                'etage' => rand(1,20),
                'idCategorie' => rand(1,4),
                'baignoire' => rand(1,5),
                'prixBase' => rand(1,50)
            ]);
            //affichage en console
            //dd($chambre);
        }
    }
}
