# FRANCOIS Daniel
# 14/05/2021

Document 1 Page 170:


Question 1: L'incendie à l'usine a été testé par:

Réponse 1:
La réparation d'une cuve de lubrifiant


Question 2: Le progrès technique et les accidents en entreprise sont-ils liés?

Réponse 2: 
Oui car: les machines peuvent entraîner des explosions ou des dysfonctionnements et le progrès technique entraîné des accidents.


Question 3: Qui a été touché par cet incendie?

Réponse 3: 
Le plombier et les salariés

Question 4: De manière plus générale, qui sont les victimes de ces accidents?

Réponse 4:

Les actionnaires -> Baisse d'activité

Les clients -> Retard de livraison

Les citoyens -> Pollution

Les salariés -> Blessures Chômage

Les fournisseurs -> Chômage Retard de livraison

L'entreprise -> Baisse d'activité


Document 2 Page 170:


Question 5: Comment s'est-il traduit le piratage informatique?

Réponse 5:

Le chef d'entreprise a été racketté

Les données de l'ordinateur ne sont plus accessibles

L'activité de l'entreprise est interrompue


Question 6: Pourquoi les cyberattaques coûtent chères aux entreprises?

Réponse 6:

L'entreprise doit faire appel à des spécialistes techniques pour résoudre les problèmes

Les cyberattaques entraînent des pertes d'activité.



Document 3P170:


Question 7: Quels sont les risques physiques des salariés?

Réponse 7:

Les manutentions manuelles

Les chuts des salariés

Les blessures résultant de l'utilisation de divers outillages


Question 8: Pourquoi ces risques impactent la vie de l'entreprise?

Réponse 8:

Ils entraînent des absences des salariés

Ils entraînent une baisse de l'activité

Ils entraînent une augmentation des cotisations sociales (accident du travail)

Ils donnent une mauvaise image de l'entreprise

Document 4 page 171:


Question 9: Quelles sont les contraintes imposées à l'employé du fait de son obligation de sécurité?

Réponse 9:

L'employeur doit veiller à la santé de son personnel

L'employeur doit respecter les règles d'aménagement des locaux

L'employeur doit évaluer les risques professionnels des postes de travail

L'employeur doit veiller à la sécurité au travail de ses salariés


Question 10: Les risques pour la santé des salariés sont-ils plus faciles à prévenir que les risques couruspar l'entreprise?

Reponse 10:

Les deux types de risques sont aussi difficiles à prévenir l'un que l'autre

Document 5 Page 171:


Question 11: Que sont les risques psychosociaux?

Réponse 11: 

Ce sont des symptômes qui affectent la psychologie et la santé mentale des salariés


Question 12: Comment se manifestent les troubles psychosociaux?

Réponse 12: Fatigue, Angoisse, Stress et Dépression


Question 13: Les risques psychosociaux ont un impact sur

Réponse 13: La santé mentale des salariés, la santé physique des salariés, l'efficience de l'entreprise et le fonctionnement de l'entreprise
