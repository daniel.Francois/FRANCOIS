#FRANCOIS DANIEL

#1/10/2020

Étape 1

1)Quelles est le masque sous réseau de chaque machine?
255.255.0.0/16

2)À quuel sous-réseau IP appartient chaque machines?Justifier
M1=192.68.0.0/16
M2=192.68.0.0/16
M3=192.68.0.0/16

3)Peuvent-elles potentiellement communiquer entre elles?Justifier
Les machines peuvent se communiquer entre elles si il y a un switch

4)Peuvent-elles dans les conditions physiques présentées ci-dessus communiquer entre elles ? Justifier.
Non

5)Quelle commande sur le terminal Filius permet de voir la configuration IP d’une STA ?
la commande ipconfig permet de voir la configuration IP d'une STA

6)Quelle commande permet de tester que les messages IP sont bien transmis entre deux machines ?
La commande ping permet de tester que les messages IP sont bien transmis entre les 2 machines

Étape 2


Quel média de communication a été ajouté ? Expliquer brièvement son fonctionnement.
le media de communication qui a été ajouté c'est une switch.

Quel autre média aurait pu le remplacer (inexistant sur Filius) ? En quoi est-il différent du premier ?
un commutateur aurait pu remplacer un switch.


Est-ce que toutes les machines peuvent maintenant communiquer entre elles ? Justifier.
Oui

Si M1 veut envoyer un message à toutes les machines de son sous-réseau, quelle adresse logique peut-elle utiliser ?
l'adresse logique est 192.168.255.255/16.
 

Étape 3

À quel réseau appartient la 4e machine (M4) ? Est-ce le même que celui des Machines M1, M2 et M3 ? Justifier.
Ce n'est pas le même que celui de M1,M2,M3 car leurs adresse IP ne sont pas les mêmes


M4 peut-elle communiquer avec M1, M2 ou M3 ? Justifier.
Non sauf si on ajoute un routeur sa pourrai marché

Si nécessaire, reconfigurer M2 pour lui permettre d’échanger des messages avec M4. Quels sont les changements nécessaires ?
Pour que M2 puisse échanger des messages avec M4, il faut changer son adresse IP 192.168.0.10, par exemple en 192.168.9.2 pour qu'elle puisse communiquer entre elle.

Est-ce que M1 et M2 peuvent toujours communiquer ? Justifier
M1 et M2 ne peuvent pas communiquer entre elles car M1 n'a pas le même réseau que M2.