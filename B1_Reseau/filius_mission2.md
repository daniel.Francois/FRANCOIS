#FRANCOIS Daniel

#5/11/2020


Étape 1
1) Quel est le CIDR de chaque machine? Justifier.
CIDR M1: 192.168.10.10/16  CIDR M2: 10.0.1.255/8  CIDR M3: 10.0.1.245/8 Car les 8 derniers bits sont aloués aux hôtes. CIDR M4: 192.168.20.10/16 

2) A quel sous-réseau IP appartient chacune de ces 4 machines? Justifier. 
Sous-réseau M1: 192.168.0.0 Car l'adresse de sous-réseau doit être la plus basse après les bits aloués à la machine, là le CIDR est de 16 Sous-réseau M2: 10.0.0.0 Car l'adresse de sous- réseau doit être la plus basse après les bits aloués à la machine, là le CIDR est de 8 Sous-réseau M3: 10.0.0.0 Car l'adresse de sous-réseau doit être la plus basse après les bits aloués à la machine, là le CIDR est de 8 Sous-réseau M4: 192.168.0.0 Car l'adresse de sous-réseau doit être la plus basse après les bits aloués à la machine, là le CIDR est de 16
3) Combien de machines maximums peuvent accueillir chacun des sous-réseaux identifiés?
M1 et M4 peuvent acceuillir jusqu'a 65534 machines car 2 ^6-2 (M2 16777214 2^16777214 machines

4) Quelles machines peuvent éventuellement communiquer entre elles? Justifier.
Les machines qui peuvent communiquer entre elles sont celles qui sont connectées au même sous-réseau que ce soit les machines M1 et M4 et les machines M2 et M3.

5) Peuvent-elles dans les conditions physiques présentées ci-dessus communiquer entre elles? Justifier.
Oui car elles sont toutes reliées entre elles par des switchs.

6) Quelle est la commande complète qui permet à M1 de tester la liaison avec M2?
La commande est ping suivi de l'adresse IP de la machine ciblée.

Étape2
1) Quelles sont les adresses IP possibles pour M5 et M6?
Il faut que M5 ait les mêmes 16 premiers bits que les machines M1 et M4 qui appartiennent au réseau 1. Il faut que M6 ait les mêmes 8 premiers bits que les machines M2 et M3 qui appartiennent au réseau 2.

2) Combien de machines peut-on encore ajouter dans chaque sous-réseau?
on peut encore ajouter ajouter 16 777 214 machines dans M5 et 65 534 dans M6

3) Si M6 veut envoyer un message à toutes les machines de son sous-réseau, quelle adresse IP peut-elle utiliser?
M6 peut utiliser l'adresse ip 10.0.255.255 pour envoyer un message à toutes les machines de son sous-réseau.

4) Quel média d'interconnexion est nécessaire pour permettre à toutes les machines d'échanger des messages?
Un routeur est nécessaire car toutes les machines ne sont pas la même adresse ip et donc ne peuvent pas toutes communiquer entre elles.

Étape3
1) Expliquez brièvement le fonctionnement d'un routeur.
Un routeur permet à toutes les machines de communiquer entre elles via un commutateur même si elles n’ont pas la même adresse ip.

2) Compléter la configuration physique pour permettre aux différentes machines des différents réseaux logiques de communiquer. • Combien d'interfaces réseaux sont nécessaires?
Il est nécessaire d'avoir 2 interfaces réseaux pour les commutateurs.
• Quelle adresse IP aura chaque interface?
La première interface aura l'adresse ip 192.168.0.1 et la deuxième 10.0.0.1.

3) Est-ce que toutes les machines peuvent maintenant communiquer entre elles? Justifier.
Oui car elles sont à présent toutes reliées entre elles par un routeur via un switch.

4) Quelle nouvelle configuration est nécessaire pour permettre aux machines de communiquer avec des machines appartenant à d'autres réseaux (M5 avec M6 par exemple)?
La nouvelle configuration qui va permettre de communiquer avec des machines appartenant à un autre réseau est de faire la liason des 2 switch et les connecteurs au routeur de manière physique
