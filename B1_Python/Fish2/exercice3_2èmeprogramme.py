bin=""
n=int(input("entrez un nombre décimal"))
if n>128:
    n=n-128  #pour faire une soustraction le chiffre qu'on a choisi par 128
    bin="1"
else:
    bin="0"
if n>64:
    n=n-64      #le résultat -64
    bin=bin+"1"
else:
    bin=bin+"0"
if n>32:
     n=n-32
     bin=bin+"1"
else:
    bin=bin+"0"
if n>16:
    n=n-16
    bin=bin+"1"
else:
    bin=bin+"0"
if n>8:
    n=n-8
    bin=bin+"1"
else:
    bin=bin+"0"
if n>4:
    n=n-4
    bin=bin+"1"
else:
    bin=bin+"0"
if n>2:
    n=n-2
    bin=bin+"1"
else:
    bin=bin+"0"
if n>1:
    n=n-1
    bin=bin+"1"
else:
    bin=bin+"0"
print(bin)