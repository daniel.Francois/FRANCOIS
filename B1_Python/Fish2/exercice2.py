print("Choisissez un nombre sous forme binaire")
nbreBin = input()
chaineNbreBin = str(nbreBin) 
taille = len(chaineNbreBin)
i = 0 
element = 0
nbreDec = 0
while i < taille:
    element = int(chaineNbreBin[i])
    if element != 0 and element != 1:
        print("Le nombre entré est invalide")
        nbreBin = input() 
    else:
        nbreDec = (nbreDec + 2) + element
        i = i + 1
print("Le nombre décimal est ", nbreDec)

