import java.util.*;


public class AdressePostale {
    private static String ville;
    private static String codepostal;
    private static String libelle;

    public AdressePostale (){}


    public AdressePostale(String ville, String codepostal, String libelle ) {
        this.ville =ville;
        this.codepostal= codepostal;
        this.libelle= libelle;

        }


     public void afficher(){
         System.out.println(this.ville + " "+ this.codepostal + " "+ this.libelle);
        }    


    public String getVille() {
        return this.ville;
    }

    public String getCodepostal() {
        return this.codepostal;
    }

    public String getLibelle() {
        return this.libelle;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public void setCodepostal(String codepostal) {
        this.codepostal = codepostal;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
  


///////----------------------
public static void main (String[] args){


    System.out.println(" votre numero de rue et votre rue : ");
    Scanner sc = new Scanner(System.in);
    String ville=sc.nextLine();

    System.out.println(" votre Ville: ");
    String codepostal=sc.nextLine();

    System.out.println(" votre Code Postal : ");
    String libelle=sc.nextLine();
    

    AdressePostale monAdressePostale = new AdressePostale(ville,codepostal,libelle);

    monAdressePostale.afficher();

    AdressePostale AdressePostale2 = new AdressePostale();
    AdressePostale2.setVille("32 bis avenue de l'enclos");
    AdressePostale2.setCodepostal("cergy");
    AdressePostale2.setLibelle("95800");
    AdressePostale2.afficher();
       
  
  }//main
}
