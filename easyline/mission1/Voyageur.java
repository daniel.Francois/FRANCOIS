import java.util.Scanner;
public class Voyageur {
    private String nom;
    private int age;
    private String categorie;

    public Voyageur(){}

    public Voyageur(String nom, int age){
        this.nom=nom;
        this.age=age;
        Scanner sc=new Scanner(System.in);
        while(nom.length()<2){
            System.out.println("ressaisir le nom :");
            nom=sc.next();
        }//while
        while ( age < 0){
            System.out.print("Veuillez taper un âge positif : ");
            age=sc.nextInt();
        }//while
        this.setCategorie();
    }//Voyageur
        public void afficher(){
        System.out.println(this.nom +" "+ this.age +" "+ this.categorie);
        }//afficher      
    
    public String getNom(){
        return this.nom;
    }//getNom
    public int getAge() {
        return this.age;
    }//getAge

    public void setNom(String nom){
        Scanner sc=new Scanner(System.in);
        while(nom.length()<2){
            System.out.println("ressaisir le nom avec deux caractères min :");
            nom=sc.next();
        }//while
        this.nom=nom;
    }//setNom
    public void setAge(int age) {
        Scanner sc=new Scanner(System.in);
        while (age < 0) {
            System.out.print("Votre âge est négatif, veuillez taper un âge positif : ");
            age=sc.nextInt();
        }//while
        this.age = age;
        setCategorie();
    }//setAge
    public void setCategorie() {
        if (age < 1) {
            this.categorie=("nourisson");
        }//if
        else if ( age < 18) {
            this.categorie=("enfant");
        }//else if
        else if (age < 60) {
            this.categorie=("adulte");
        }//else if
        else{
            this.categorie=("senior");
        }//else if
    }//setCategorie
    
    public String getCategorie(){
        return this.categorie;
    }//getCategorie
    public static void main(String[] args){
        Scanner sc=new Scanner(System.in);
        System.out.println("Saisissez le nom :");
        String nom=sc.next();
        
        

        System.out.print("Saisissez l'age :");
        int age=sc.nextInt();
        
        Voyageur monVoyageur = new Voyageur(nom, age);
        monVoyageur.afficher();

        Voyageur monVoyageur2 = new Voyageur();
        monVoyageur2.setNom("Daniel");
        monVoyageur2.setAge(18);
        monVoyageur2.afficher();

     }//main 
}//class

