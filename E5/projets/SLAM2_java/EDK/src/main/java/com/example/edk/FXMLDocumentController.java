package com.example.edk;



import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;


import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 *
 * @author Bushan Sirgur
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private TextField textIdentifiant;

    @FXML
    private PasswordField textPassword;

    @FXML
    private Button signUpButton;

    Stage dialogStage = new Stage();
    Scene scene;





    Connection connection = null;
    PreparedStatement preparedStatement = null;
    ResultSet resultSet = null;
    private Stage stage;

    public FXMLDocumentController() {
        connection = ConnectionUtil.connectdb();
    }

    @FXML
    private void handleLoginAction(ActionEvent event) {
        String login = textIdentifiant.getText().toString();
        String password = textPassword.getText().toString();

        String sql = "SELECT * FROM user_edk WHERE login = ? and password = PASSWORD(?)";

        try{
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, login);
            preparedStatement.setString(2, password);
            resultSet = preparedStatement.executeQuery();
            if(!resultSet.next()){
                infoBox("Enter Correct Email and Password", "Failed", null);
            }else{
                infoBox("Login Successfull", "Success", null);
                Node source = (Node) event.getSource();
                dialogStage = (Stage) source.getScene().getWindow();
                dialogStage.close();
                scene = new Scene(FXMLLoader.load(getClass().getResource("Accueil.fxml")));
                dialogStage.setScene(scene);
                dialogStage.show();
            }

        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public static void infoBox(String infoMessage, String titleBar, String headerMessage)
    {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle(titleBar);
        alert.setHeaderText(headerMessage);
        alert.setContentText(infoMessage);
        alert.showAndWait();
    }

    @FXML

    public void handleInscriptionAction(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        dialogStage = (Stage) source.getScene().getWindow();
        dialogStage.close();
        scene = new Scene(FXMLLoader.load(getClass().getResource("Inscription.fxml")));
        dialogStage.setScene(scene);
        dialogStage.show();
    }


    public void handleRoleAction(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        dialogStage = (Stage) source.getScene().getWindow();
        dialogStage.close();
        scene = new Scene(FXMLLoader.load(getClass().getResource("Inscription.fxml")));
        dialogStage.setScene(scene);
        dialogStage.show();
    }


    @Override
    public void initialize(URL url, ResourceBundle rb) {



    }
}