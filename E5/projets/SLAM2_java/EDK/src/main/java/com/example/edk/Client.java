package com.example.edk;

public class Client {

    private int id;
    private String nom;
    private String prenom;
    private String adresse;
    private String ville;
    private String accessoire;



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }



    public String getNom() {

        return nom;
    }

    public void setNom(String VisualiserNom) {

        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }


    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getAccessoire() {
        return accessoire;
    }

    public void setAccessoire(String accessoire) {
        this.accessoire = accessoire;
    }

    public Client (int id , String nom ,String prenom, String adresse , String ville , String accessoire){
        this.id = id ;
        this.nom = nom ;
        this.prenom = prenom ;
        this.adresse = adresse ;
        this.ville = ville ;
        this.accessoire =accessoire;


    }


}


