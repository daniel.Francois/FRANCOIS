package eu.hautil;

import java.sql.*;
import java.util.Scanner;

public class Fonction {
    //aller à la page https://dev.mysql.com/doc/connector-j/5.1/en/connector-j-usagenotes-connect-drivermanager.html
    String driver="com.mysql.cj.jdbc.Driver"; // le site de mysql
    String urlBDD="jdbc:mysql://localhost:3306/who"; // le site de mysql pour voir exemple
    String loginBDD="root"; // mysql local
    String mdpBDD="root"; //mysql local
    Scanner sc = new Scanner(System.in);

    String requete1 = "SELECT id_role, libelle FROM who_roles";
    String requete2 = "INSERT INTO who_roles (id_role, libelle) VALUES (?,?)";
    String requete3 = "DELETE FROM `who_roles` WHERE id_role=?";
    String requete4 = "SELECT libelle FROM who_roles where id_role = ?";
    String requete5 = "SELECT id,login,mdp,role,date_creation,nom,prenom,id_role,libelle FROM who_utilisateurs INNER JOIN who_roles on role = id_role";
    String requete6 = "SELECT id,login,mdp,role,date_creation,nom,prenom,id_role,libelle FROM who_utilisateurs INNER JOIN who_roles on role = id_role WHERE login = ?";
    String requete7 = "INSERT INTO who_utilisateurs (login,mdp,role,date_creation,nom,prenom) VALUES (?,PASSWORD(?),?,?,?,?)";
    String requete8 = "DELETE FROM who_utilisateurs WHERE login = ?";




    private Connection getConnection() throws ClassNotFoundException, SQLException {
        Class.forName(driver);
        Connection conn= DriverManager.getConnection(urlBDD,loginBDD,mdpBDD);
        System.out.println("Connexion ok " + conn);

        Statement stmt = conn.createStatement();

        return conn;
    }
    public void afficherRoles() throws SQLException {
        Connection conn= DriverManager.getConnection(urlBDD,loginBDD,mdpBDD);
        Statement stmt = conn.createStatement();
        ResultSet result = stmt.executeQuery(requete1);

        while (result.next()) {
            System.out.print("\n id: " + result.getString(1));
            System.out.print("\n libelle: " + result.getString(2) + "\n");
        }//while
    }//afficherRoles

    public void ajouterRoles(int id, String libelle) throws SQLException{
        Connection conn= DriverManager.getConnection(urlBDD,loginBDD,mdpBDD);
        Statement stmt = conn.createStatement();
        PreparedStatement pstmt = conn.prepareStatement(requete2);

        pstmt.setInt(1,id);
        pstmt.setString(2,libelle);

        int res1 = pstmt.executeUpdate();

        System.out.println(res1 + " : Ligne bien insérées");
    }//ajouterRoles

    public void supprimerRoles(int id) throws SQLException {

        Connection conn = DriverManager.getConnection(urlBDD, loginBDD, mdpBDD);
        Statement stmt = conn.createStatement();
        PreparedStatement pstmt2 = conn.prepareStatement(requete3);
        pstmt2.setInt(1, id);

        int res2 = pstmt2.executeUpdate();
        System.out.println(res2 + " : Ligne bien supprime");


    }//supprimerRoles

    public void afficherRoleById(int id) throws SQLException{
        Connection conn= DriverManager.getConnection(urlBDD,loginBDD,mdpBDD);

        PreparedStatement pstmt3 = conn.prepareStatement(requete4);
        pstmt3.setInt(1, id);
        ResultSet res3 = pstmt3.executeQuery();
        while (res3.next()) {
            System.out.print("\n id: " + res3.getString(1));
        }//while
        res3.close();
    }//afficherRolesById


    public void afficherUtilisateurs() throws SQLException {
        Connection conn = DriverManager.getConnection(urlBDD, loginBDD, mdpBDD);
        Statement stmt = conn.createStatement();
        ResultSet firstTable = stmt.executeQuery(requete5);
        System.out.println("\nVoici la table après vos modifications : ");

        while (firstTable.next()) {
            System.out.print("Id : " + firstTable.getString(1) + " | ");
            System.out.print("Login : " + firstTable.getString(2) + " | ");
            System.out.print("Mdp : " + firstTable.getString(3) + " | ");
            System.out.print("Rôle : " + firstTable.getString(4) + " | ");
            System.out.print("Date Création : " + firstTable.getString(5) + " | ");
            System.out.print("Nom : " + firstTable.getString(6) + " | ");
            System.out.print("Prénom : " + firstTable.getString(7) );
            System.out.print("Rôle ID : " + firstTable.getString(8) + " ");
            System.out.print("Libellé Rôle : " + firstTable.getString(9) + "\n");
        }
    }

    public void afficherUtilisateursByLogin(String login) throws SQLException {
        Connection conn = DriverManager.getConnection(urlBDD, loginBDD, mdpBDD);
        PreparedStatement pstmt2 = conn.prepareStatement(requete6);
        pstmt2.setString(1, login);

        ResultSet res4 = pstmt2.executeQuery();

        while (res4.next()) {
            System.out.print("Id : " + res4.getString(1) + " | ");
            System.out.print("Login : " + res4.getString(2) + " | ");
            System.out.print("Mdp : " + res4.getString(3) + " | ");
            System.out.print("Rôle : " + res4.getString(4) + " | ");
            System.out.print("Date Création : " + res4.getString(5) + " | ");
            System.out.print("Nom : " + res4.getString(6) + " | ");
            System.out.print("Prénom : " + res4.getString(7) + "");
            System.out.print("Rôle ID : " + res4.getString(8) + " ");
            System.out.print("Libellé Rôle : " + res4.getString(9) + "\n");

        }
        res4.close();
    }


    public void ajouterUtilisateur(String login, String mdp, int role, String date_creation, String nom, String prenom) throws SQLException {
        Connection conn = DriverManager.getConnection(urlBDD, loginBDD, mdpBDD);
        Statement stmt = conn.createStatement();
        PreparedStatement pstmt = conn.prepareStatement(requete7);

        pstmt.setString(1, login);
        pstmt.setString(2, mdp);
        pstmt.setInt(3, role);
        pstmt.setString(4, date_creation);
        pstmt.setString(5, nom);
        pstmt.setString(6, prenom);

        int res5 = pstmt.executeUpdate();
        System.out.println("Voici la table après l'insertion des données : "+ res5);


        conn.close();


    }

    public void SupprimerUtilisateur(String login) throws SQLException{
        Connection conn = DriverManager.getConnection(urlBDD, loginBDD, mdpBDD);
        Statement stmt = conn.createStatement();
        PreparedStatement pstmt = conn.prepareStatement(requete8);
        pstmt.setString(1, login);

        int res6 = pstmt.executeUpdate();

        System.out.println("Voici la table après la suppression des données : " + res6 );

    }
}







//Fonction


