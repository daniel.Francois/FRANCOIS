<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\auth;
use App\Http\Controllers\PremierController;
use App\Http\Controllers\ChambreController;
use App\Http\Controllers\ReservationController;
use App\Http\Controllers\HaraController;
use App\Http\Controllers\FormulaireaController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/compte', function () {
    return view('auth/compte');
});



/*Route::get('/accueil', function () {
    return view('accueil');
});*/

Route::get('/accueil',[PremierController::class, 'home'] );
Route::get('/monUriVersPremierControllerFunctionhome',[PremierController::class, 'home'] );

Route::get('/chambre',[ChambreController::class, 'store'] );



Route::get('/newreservation',[ReservationController::class, 'create'] )->middleware('auth');

Route::get('/failure',function () {
    return view('failure');
})->name('failure');

Route::post('/storereservation',[ReservationController::class, 'store'] )->name('reservation.store');



Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::get('/deconnexion',function () {
    Auth::logout();
     return redirect('/accueil');
 });


 Route::get('/Hara',[HaraController::class, 'create'] );

 Route::get('/Formulaire',[FormulaireController::class, 'create'] );