# FRANCOIS Daniel
# 11/03/2021

1.	Que signifie le sigle GLPI ?


Le sigle GLPI signifie Gestionnaire Libre de Parc Informatique
2.	Qu'est-ce que GLPI ?


Le GLPI est un logiciel libre de gestion des services informatiques et une application web qui aide les entreprises à gérer leur système d'information.
3.	Citez les principaux modules qui composent l'application GLPI.


Le module Parc 


Le module Assistance


Le module Gestion


Le module Outils


Le module Administration


Le module Configuration

4.	Décrivez les modules cités dans la question précédente (question n ° 3).


Le module Parc permet d'accéder aux différents matériels inventoriés. 


Le module Assistance permet de créer, suivre les tickets, et voir les statistiques. 


Le module Gestion permet de gérer les contacts, fournisseurs, budgets, contrats et documents. 


Le module Outils permet de gérer des notes, la base de connaissance, les réservations, les flux RSS et visualiser les rapports. 


Le module Administration permet d'administrer les utilisateurs, groupes, entités, profils, règles et dictionnaires. Il permet aussi la maintenance de 
l'application (sauvegarde et restauration de base, vérifier si une nouvelle version est disponible). 


Le module Configuration permet d'accéder aux options de configuration générale de GLPI : notifications, collecteurs, tâches automatiques, authentification, plugins, liens externes. 

5.	Quels sont, d'après vous, les avantages que peut présenter cette application?


A gérer leur système d’information, la sécurité, réduire les couts.

6.	Trouvez-vous des solutions similaires à GLPI, présentes sur le marché ?
TOPdesk ,Automox,ChangeGear,OTRS



Source :
https://fr.wikipedia.org/wiki/Gestionnaire_Libre_de_Parc_Informatique#:~:text=GLPI%20(Gestionnaire%20Libre%20de%20Parc,issue%20tracking%20system%20et%20ServiceDesk).&text=GLPI%20est%20une%20application%20web,g%C3%A9rer%20leur%20syst%C3%A8me%20d'information.


https://glpi-project.org/DOC/FR/glpi/navigate.html

https://www.getapp.fr/directory/292/it-service-management-itsm/software
