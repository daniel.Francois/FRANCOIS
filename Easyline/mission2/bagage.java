
public class bagage {
    private String couleur;
    private int numero;
    private Double poids;

    public bagage(){}

    public bagage(String couleur, int numero, Double poids ) {
        this.couleur=couleur;
        this.numero=numero;
        this.poids=poids;
           }    
   
   
       public String getCouleur() {
           return this.couleur;
       }
   
       public int getNumero() {
           return this.numero;
       }
   
       public Double getPoids() {
           return this.poids;
       }
   
       public void setCouleur(String couleur) {
           this.couleur = couleur;
       }
   
       public void setNumero(int numero) {
           this.numero = numero;
       }
   
       public void setPoids(Double poids) {
           this.poids = poids;
       }

       @Override
       public String toString() {
           
        return this.numero+" "+this.couleur+" "+this.poids;
       }

        }

}
