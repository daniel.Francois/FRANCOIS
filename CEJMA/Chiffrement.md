Première partie : Cryptographie symétrique 


Questions :


Quels sont les caractéristiques de l’algorithme AES (année, avantages, inconvénients, usages recommandé, comparaison avec 3DES) ?
Les caractéristiques de l’algorithme AES sont:


•	le 2 janvier 1997
(Année)


•	sécurité ou l'effort requis pour une éventuelle cryptanalyse.
facilité de calcul : cela entraine une grande rapidité de traitement
besoins en ressources et mémoire très faibles flexibilité d'implémentation: cela inclut une grande variété de plateformes et d'applications ainsi que des tailles de clés et de blocs supplémentaires (c.f. ci-dessus).
hardware et software : il est possible d'implémenter l'AES aussi bien sous forme logicielle que matérielle (câblé)
simplicité : le design de l'AES est relativement simple
(Avantages)


•	Il faudra partager la phrase de passe d’une manière ou d’une autre donc risque de la divulguer et de compromettre potentiellement le message chiffré.
(inconvéniants)


•	Remplacer le DES (Data Encryption Standard) qui est devenu trop faible au regard des attaques actuelles.
(usage recommandé)


•	L'AES est plus sûr que le 3DES car il présente, entre autres, une plus grande résistance aux attaques par dictionnaires de clés. Les autres attaques ne sont pas appliquables dans son cas.
(comparaison avec 3DES)
Quels sont les critères à prendre en compte pour déterminer le niveau de sécurité dont une organisation a besoin ?


•	sécurité ou l'effort requis pour une éventuelle cryptanalyse.


•	facilité de calcul : cela entraine une grande rapidité de traitement


•	besoins en ressources et mémoire très faibles


•	flexibilité d'implémentation: cela inclut une grande variété de plateformes et d'applications ainsi que des tailles de clés et de blocs supplémentaires (c.f. ci-dessus).


•	hardware et software : il est possible d'implémenter l'AES aussi bien sous forme logicielle que matérielle (câblé)


•	simplicité : le design de l'AES est relativement simple
Quelles sont les recommandations de l’ANSSI sur la longueur des clés symétriques ?


•	La taille recommandée pour les systèmes symétriques est de 128 bits.
Présentez un exemple d’utilisation de l’algorithme AES.


•	WhatsApp utilise l'algorithme AES.
Deuxième partie : Cryptographie asymétrique (à clé publique) 
Quels sont noms des inventeurs de l’algorithme de chiffrement RSA et à quelle date ont-ils publiés le principe de cet algorithme ?


•	Cet algorithme a été décrit en 1977 par Ronald Rivest, Adi Shamir et Leonard Adleman. RSA a été breveté par le Massachusetts Institute of Technology (MIT) en 1983 aux États-Unis. Le brevet a expiré le 21 septembre 2000.
Quels sont les deux usages de ce chiffrement asymétrique ?


•	Le système RSA est utilisé à la fois pour crypter des messages (la confidentialité) et pour les signer (la non-altération et la non-répudiation).
Quelle est la longueur des clés que recommande l’ANSSI en France pour la période actuelle ?


•	La taille minimale des clés symétriques utilisées pour la période actuelle est de 100 bits.
A quel usage est initialement destiné l’algorithme de chiffrement DSA ?


•	Le Digital Signature Algorithm, plus connu sous le sigle DSA, est un algorithme de signature numérique standardisé par le NIST aux États-Unis
Indiquez les différentes étapes nécessaires à la création de ce tunnel de communication sécurisé SSH.


•	Le serveur envoie sa clé publique au client.


•	Le client génère une clé secrète et l'envoie au serveur, en cryptant l'échange avec la clé publique du serveur (cryptographique asymétrique). Le serveur décrypte la clé secrète en utilisant sa clé privée, ce qui prouve qu'il est bien le vrai serveur.


•	Pour le prouver au client, il crypte un message standard avec la clé secrète et l'envoie au client. Si le client retrouve le message standard en utilisant la clé secrète, il a la preuve que le serveur est bien le vrai serveur.


•	Une fois la clé secrète échangée, le client et le serveur peuvent alors établir un canal sécurisé grâce à la clé secrète commune (cryptographie symétrique).


•	Une fois que le canal sécurisé est en place, le client va pouvoir envoyer au serveur le login et le mot de passe de l'utilisateur pour vérification. La canal sécurisé reste en place jusqu'à ce que l'utilisateur se déloggue.
Comment peut-on s’authentifier par clé pour le service SSH ?


•	Au lieu de s'authentifier par mot de passe, les utilisateurs peuvent s'authentifier grâce à la cryptographie asymétrique et son couple de clés privée/publique, comme le fait le serveur SSH auprès du client SSH.
Troisième partie : Signature numérique 
Quelles autres fonctions de hachage est-il conseillé d’utiliser à la place de MD5 ?
Identifier deux usages des fonctions de hachage.
Présentez une offre de gestion de la signature électronique :
o Objectif de la réglementation européenne eIDAS
o Exigences de fiabilité de la signature électronique
o Les différents niveaux de la signature électronique du règlement eIDAS
o Les enjeux
o Quelques usages de la signature électronique

