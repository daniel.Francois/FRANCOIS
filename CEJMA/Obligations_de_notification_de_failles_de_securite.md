# FRANCOIS Daniel
# 9/03/2021



1)Les differents types de failles de securite

1.La messagerie d’entreprise
2.La gestion des accès
3 Les logiciels obsolètes
4 Le télétravail et l’utilisation de matériel personnel
5 L’hébergement des données

2)Le processus de notification prévus par la RGPD
Diffrentes notifications 
A qui les adresser?
Quand y-a-t-il sanction de la CNIL?
Amandes maximales prevues


Lorsque des manquements au RGPD ou à la loi sont portés à sa connaissance, la formation restreinte de la CNIL peut :

    Prononcer un rappel à l’ordre ;
    Enjoindre de mettre le traitement en conformité, y compris sous astreinte ;
    Limiter temporairement ou définitivement un traitement ;
    Suspendre les flux de données ;
    Ordonner de satisfaire aux demandes d'exercice des droits des personnes, y compris sous astreinte ;
    Prononcer une amende administrative.




Avec le RGPD (Règlement Général sur la Protection des Données), le montant des sanctions pécuniaires peut s'élever jusqu'à 20 millions d'euros ou dans le cas d'une entreprise jusqu'à 4 % du chiffre d'affaires annuel mondial




il faut s'adresser à la CNIL

3)La politique interne concernant les failles de securité


Cahier des incidents  


modalité de communication
...




4) 2 Exemples reel de failles de securite entreprise concernée
Date?
QUelles failles 
Date de notifications
sanction(eventuelle, de la CNIL)



Exemple d'entreprise: UBER

Le système Uber est un système de transport rémunéré de particulier à particulier via une application pour smartphones. Ce sont donc les chauffeurs qui sont ses clients. Les utilisateurs du système le jugent moins cher que les taxis traditionnels.


Le 21 novembre 2017,
Uber reconnaissait avoir fait l'objet d'un vol de données concernant 57 millions de ses utilisateurs, en 2016.
Amende:La Cnil a décidé de prononcer une sanction de 400.000 euros à l'encontre d'Uber.


Exemple d'entreprise:SERGIC

La société SERGIC est spécialisée dans la promotion immobilière, l’achat, la vente, la location et la gestion immobilière.
En août 2018,

la CNIL a reçu une plainte d’un utilisateur du site indiquant avoir pu accéder, depuis son espace personnel sur le site, à des documents enregistrés par d’autres utilisateurs en modifiant légèrement l’URL affichée dans le navigateur.
Amende

la CNIL a prononcé une sanction de 400 000 euros à l’encontre de la société SERGIC pour avoir insuffisamment protégé les données des utilisateurs de son site web .
