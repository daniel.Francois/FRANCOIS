module eu.hautil.demojavafx {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;

    opens eu.hautil.demojavafx to javafx.fxml;
    exports eu.hautil.demojavafx;
}