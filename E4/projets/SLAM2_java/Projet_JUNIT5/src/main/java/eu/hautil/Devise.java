package eu.hautil;

public class Devise {
    private int quantite;
    private String monnaie;

    public Devise(int somme, String monnaie) {
        this.quantite = somme;
        this.monnaie = monnaie;
    }

    public int getQuantite() {
        return quantite;
    }

    public String getMonnaie() {
        return monnaie;
    }

    public Devise add(Devise m) throws MonnaieDifferenteException {
        if (this.getMonnaie() == m.getMonnaie()) {
            System.out.println(this.getMonnaie());
            System.out.println(m.getMonnaie());
            return new Devise(getQuantite() + m.getQuantite(), getMonnaie());
        } else {
            throw new MonnaieDifferenteException(this, m);
        }


    }


    @Override
    public String toString() {
        return "Devise{" +
                "quantite=" + quantite +
                ", monnaie='" + monnaie + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        // A Compléter

        if (obj instanceof Devise) {
            Devise o = (Devise) obj; //Permet d'utiliser les méthodes de devise
            o.getMonnaie();


            if (this.getMonnaie() == o.getMonnaie() && this.getQuantite() == o.getQuantite()) {
                return true;
            }
        }
        return false;
    }

}
