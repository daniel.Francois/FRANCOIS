package eu.hautil;

import java.sql.*;
import java.util.Scanner;

public class Fonction {
    //aller à la page https://dev.mysql.com/doc/connector-j/5.1/en/connector-j-usagenotes-connect-drivermanager.html
    String driver="com.mysql.cj.jdbc.Driver"; // le site de mysql le driver est
    String urlBDD="jdbc:mysql://localhost:3306/who"; // le site de mysql pour voir exemple
    String loginBDD="root"; // mysql local
    String mdpBDD="root"; //mysql local
    Scanner sc = new Scanner(System.in);

    String req1 = "SELECT id_role, libelle,  FROM who_utlisateur";//choisir dans table who_roles les élément id_role, libelle
    String req2 = "INSERT INTO who_roles (id_role, libelle) VALUES (?,?)";
    String req3 = "DELETE FROM `who_roles` WHERE id_role=?";
    String req4 = "SELECT libelle FROM who_roles where id_role = ?";
    String req5 = "SELECT login, mdp,  FROM who_utlisateur";

    private Connection getConnection() throws ClassNotFoundException, SQLException { //ne trouve pas le driver
        Class.forName(driver);
        Connection conn= DriverManager.getConnection(urlBDD,loginBDD,mdpBDD);
        System.out.println("Connexion ok " + conn);

        Statement stmt = conn.createStatement();

        return conn;
    }
    public void afficherRoles() throws SQLException {
        Connection conn= DriverManager.getConnection(urlBDD,loginBDD,mdpBDD);
        Statement stmt = conn.createStatement();
        ResultSet result = stmt.executeQuery(req1);

        while (result.next()) {
            System.out.print("\n id: " + result.getString(1));
            System.out.print("\n libelle: " + result.getString(2) + "\n");
        }//while
    }//afficherRoles

    public void ajouterRoles(int id, String libelle) throws SQLException{
        Connection conn= DriverManager.getConnection(urlBDD,loginBDD,mdpBDD);
        Statement stmt = conn.createStatement();
        PreparedStatement pstmt = conn.prepareStatement(req2);

        pstmt.setInt(1,id);
        pstmt.setString(2,libelle);

        int res1 = pstmt.executeUpdate();

        System.out.println(res1 + " : Ligne bien insérées");
    }//ajouterRoles

    public void supprimerRoles(int id) throws SQLException {

        Connection conn = DriverManager.getConnection(urlBDD, loginBDD, mdpBDD);
        Statement stmt = conn.createStatement();
        PreparedStatement pstmt2 = conn.prepareStatement(req3);
        pstmt2.setInt(1, id);

        int res4 = pstmt2.executeUpdate();
        System.out.println(res4 + " : Ligne bien insérées");


    }//supprimerRoles

    public void afficherRoleById(int id) throws SQLException{ //si le programme est faux il fait une erreur.
        Connection conn= DriverManager.getConnection(urlBDD,loginBDD,mdpBDD);

        PreparedStatement pstmt3 = conn.prepareStatement(req4);
        pstmt3.setInt(1, id);
        ResultSet res5 = pstmt3.executeQuery();
        while (res5.next()) {
            System.out.print("\n id: " + res5.getString(1));
        }//while
        res5.close();
    }//afficherRolesById


    public void VerificationRole(string login, string mdp) throws SQLException{ //si le programme est faux il fait une erreur.
        Connection conn= DriverManager.getConnection(urlBDD,loginBDD,mdpBDD);

        PreparedStatement pstmt3 = conn.prepareStatement(req5);
        pstmt3.setInt(1, login);
        ResultSet res5 = pstmt3.executeQuery();
        while (res5.next()) {
            System.out.print("\n Bonjour: " + res5.getString(1));
        }//while
        res5.close();
    }//afficherRolesById

}//Fonction


