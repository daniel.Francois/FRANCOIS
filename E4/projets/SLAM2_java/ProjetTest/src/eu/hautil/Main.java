package eu.hautil;

import java.sql.*;
import java.util.Scanner;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Main {

    public static void main(String[] args) {
        Fonction accesRole = new Fonction();

        try {

            System.out.println("\n" +"Bienvenue dans le Main.");

            Scanner sc = new Scanner(System.in);
            int choice = 1;

            while(choice>=1 && choice<=8){
                System.out.print("\n");
                System.out.println("Choisissez une option");
                System.out.println("1: Visualiser le contenu la table \n" +
                        "2: Ajouter un élément à la table \n" +
                        "3: Supprimer un élément de la table \n" +
                        "4: Sélectionner un rôle en fonction de l'ID \n" +
                        "5: Visualiser le contenu la table utilisateurs  \n" +
                        "6: Afficher par le login\n" +
                        "7: Ajouter un utilisateur \n" +
                        "8: pour supprimer un utilisateur \n" +
                        "9: Quitter");
                System.out.print("Tapez le chiffre correspondant à votre choix: ");
                choice = sc.nextInt();
                sc.nextLine();// apres chaque n



                String login;
                String mdp;
                int role;
                String date_creation = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));//
                String nom;
                String prenom;





                switch(choice) {

                    case 1 -> accesRole.afficherRoles();


                    case 2 -> {
                        System.out.println("Renseignez l'ID : ");
                        int id = sc.nextInt();
                        sc.nextLine(); //C'est pour "aspirer" le bouton entrée qui est compté comme une valeur

                        System.out.println("Renseignez le rôle : ");
                        String libelle = sc.nextLine();

                        accesRole.ajouterRoles(id, libelle);
                    }

                    case 3 -> {
                        System.out.print(" rentrer l'ID du rôle que vous souhaitez supprimer : ");
                        int id2 = sc.nextInt();
                        accesRole.supprimerRoles(id2);

                    }


                    case 4 -> {
                        System.out.println("rentrer l'ID du rôle que vous souhaitez sélectionner : \n");
                        int id3 = sc.nextInt();
                        sc.nextLine();
                        accesRole.afficherRoleById(id3);
                    }

                    case 5 -> {
                        System.out.print("Voici la table utilisateurs : \n");
                        accesRole.afficherUtilisateurs() ;

                    }

                    case 6-> {
                        System.out.print("Entre le login : \n");

                        System.out.print("Rentrez le login de l'utilisateur que vous souhaitez afficher : ");
                        login = sc.nextLine();
                        accesRole.afficherUtilisateursByLogin(login);

                    }

                    case 7 -> {
                        System.out.print("Rentrez le login : ");
                        login = sc.nextLine();

                        System.out.print("Rentrez mot de passe : ");
                        mdp = sc.nextLine();

                        System.out.print("Rentrez le rôle (1 | 2 | 3 | 4: ");
                        role = sc.nextInt();
                        sc.nextLine();


                        System.out.print("Rentrez le nom : ");
                        nom = sc.nextLine();

                        System.out.print("Rentrez le prénom : ");
                        prenom = sc.nextLine();

                        accesRole.ajouterUtilisateur(login, mdp, role, date_creation, nom, prenom);
                    }

                    case 8 -> {
                        System.out.print("Rentrez l'ID de l'utilisateur que vous souahitez supprimer : ");
                        login = sc.nextLine();
                        accesRole.SupprimerUtilisateur(login);
                    }



                    default -> System.out.println("okkkkkkkkkkk !");




                }
            }


        } catch (SQLException e){
            e.printStackTrace();
        }
    }
}
