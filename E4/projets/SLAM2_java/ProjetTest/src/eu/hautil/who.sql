-- Adminer 4.7.7 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `who_utilisateurs`;
CREATE TABLE `who_utilisateurs` (
  `id` int(7) NOT NULL AUTO_INCREMENT,
  `login` varchar(30) NOT NULL,
  `mdp` varchar(30) NOT NULL,
  `role` int(3) NOT NULL,
  `date_creation` datetime NOT NULL,
  `nom` varchar(30) NOT NULL,
  `prenom` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`),
  KEY `role` (`role`),
  CONSTRAINT `who_utilisateurs_ibfk_1` FOREIGN KEY (`role`) REFERENCES `who_roles` (`id_role`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

INSERT INTO `who_utilisateurs` (`id`, `login`, `mdp`, `role`, `date_creation`, `nom`, `prenom`) VALUES
(1,	'xforo',	'*FE1C2745E806B636F29EF399A3610',	1,	'2021-09-22 00:00:00',	'EL MOKHTARI',	'karim'),
(2,	'bloodrinkr',	'*97E7471D816A37E38510728AEA474',	2,	'2002-10-27 00:00:00',	'FRANCOIS',	'daniel'),
(3,	'cmoamathis',	'*00A51F3F48415C7D4E8908980D443',	3,	'2015-06-05 00:00:00',	'Prince',	'mathis'),
(4,	'monster',	'*F184EEE2EEDD44947378CD8609C4C',	4,	'2005-10-14 00:00:00',	'FRANCOIS',	'adrien'),
(5,	'bonebreakr',	'*EA37F0DFF4BB13F7C47164077F29D',	4,	'2013-01-24 00:00:00',	'MITSAKIS',	'david'),
(6,	'sinister',	'*D9F1189D8F655B65B502229063D2A',	3,	'2014-09-07 00:00:00',	'HENDRIX',	'mucas'),
(7,	'hamer',	'*3E1BBB947640612968FF4A611446C',	2,	'2002-12-31 00:00:00',	'DIAVOUDINE',	'hameed'),
(8,	'thorskill',	'*03168F30E9309552174350BB4EFEC',	2,	'2021-09-21 00:00:00',	'THORS',	'kill'),
(9,	'shadow',	'*E390FBA2047B6D3A9E3CA1A8BEFB1',	1,	'2021-09-10 00:00:00',	'BONE',	'kyllian'),
(10,	'doom',	'*CA26B68C9C73627F07C0BDD61B906',	3,	'2012-05-26 00:00:00',	'Master',	'chief');

-- 2021-10-19 17:33:53

