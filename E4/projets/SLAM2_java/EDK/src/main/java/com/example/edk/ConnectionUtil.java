package com.example.edk;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.*;
import javax.swing.*;
public class ConnectionUtil {
    Connection conn = null;
    public static Connection connectdb()
    {
        try
        {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection conn = DriverManager.getConnection("jdbc:mysql://sio-hautil.eu/balane","balane","balane");
            return conn;
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, e);
            return null;
        }
    }
   public static ObservableList<Client> getDataClient() {
        Connection conn = connectdb();
        ObservableList<Client> listClient = FXCollections.observableArrayList();
        try {

            PreparedStatement ps = conn.prepareStatement("select * from client");
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {


                listClient.add(new Client(
                        rs.getInt("id"),
                        rs.getString("nom"),
                        rs.getString("prenom"),
                        rs.getString("adresse"),
                        rs.getString("ville"),
                        rs.getString("accessoire")));





            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return listClient;
    }


}

