package com.example.edk;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class ClientController implements Initializable {


    @FXML
    private TextField Ajoutnom;

    @FXML
    private TextField Ajoutprenom;

    @FXML
    private TextField Ajoutmail;

    @FXML
    private TextField  Ajoutville;

    @FXML
    private TextField  AjoutAccessoires;


    Stage dialogStage = new Stage();
    Scene scene;


    Connection connection = null;
    PreparedStatement preparedStatement = null;
    ResultSet resultSet = null;

    private Stage stage;


    public ClientController() {
        connection = ConnectionUtil.connectdb();
    }


    @FXML

    public void returntaches(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        dialogStage = (Stage) source.getScene().getWindow();
        dialogStage.close();
        scene = new Scene(FXMLLoader.load(getClass().getResource("Accueil.fxml")));
        dialogStage.setScene(scene);
        dialogStage.show();
    }

    @FXML
    private void addClient (ActionEvent event) throws IOException, SQLException {


        {
            String nom = Ajoutnom.getText().toString();
            String prenom = Ajoutprenom.getText().toString();
            String adresse = Ajoutmail.getText().toString();
            String Ville = Ajoutville.getText().toString();
            String Accessoire = AjoutAccessoires.getText().toString();

            String sql = "INSERT INTO client (nom,prenom,adresse,Ville,Accessoire)VALUES(?,?,?,?,?) ";

            try {
                preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setString(1, nom);
                preparedStatement.setString(2, prenom);
                preparedStatement.setString(3, adresse);
                preparedStatement.setString( 4, Ville);
                preparedStatement.setString( 5, Accessoire);
                preparedStatement.executeUpdate();
    

                Node source = (Node) event.getSource();
                dialogStage = (Stage) source.getScene().getWindow();
                dialogStage.close();
                scene = new Scene(FXMLLoader.load(getClass().getResource("Client.fxml")));
                dialogStage.setScene(scene);
                dialogStage.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }








            @Override
            public void initialize (URL location, ResourceBundle resources){

        }





}