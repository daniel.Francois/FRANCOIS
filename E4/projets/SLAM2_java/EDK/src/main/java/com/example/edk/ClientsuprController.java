package com.example.edk;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class ClientsuprController implements Initializable {


    @FXML
    private TextField IdClient;


    Stage dialogStage = new Stage();
    Scene scene;


    Connection connection = null;
    PreparedStatement preparedStatement = null;
    ResultSet resultSet = null;

    private Stage stage;


    public ClientsuprController() {
        connection = ConnectionUtil.connectdb();
    }



    @FXML

    public void returntache(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        dialogStage = (Stage) source.getScene().getWindow();
        dialogStage.close();
        scene = new Scene(FXMLLoader.load(getClass().getResource("Accueil.fxml")));
        dialogStage.setScene(scene);
        dialogStage.show();
    }



    @FXML
    private void SupprimerClient(ActionEvent event) throws IOException, SQLException {


        {
            String ID = IdClient.getText().toString();

            String sql = "DELETE FROM client WHERE(id=?) ";

            try{
                preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setString(1, ID);
                preparedStatement.executeUpdate();


                Node source = (Node) event.getSource();
                dialogStage = (Stage) source.getScene().getWindow();
                dialogStage.close();
                scene = new Scene(FXMLLoader.load(getClass().getResource("Clientsupr.fxml")));
                dialogStage.setScene(scene);
                dialogStage.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}