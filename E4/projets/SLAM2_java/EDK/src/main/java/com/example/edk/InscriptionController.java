package com.example.edk;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.example.edk.FXMLDocumentController.infoBox;

public class InscriptionController implements Initializable {


    @FXML
    private Button AjoutInformation;

    @FXML
    private Button retour;

    @FXML
    private TextField TextEmail;

    @FXML
    private TextField textIdentifiant;

    @FXML
    private PasswordField textPassword;


    Stage dialogStage = new Stage();
    Scene scene;


    Connection connection = null;
    PreparedStatement preparedStatement = null;
    ResultSet resultSet = null;

    private Stage stage;


    public InscriptionController() {
        connection = ConnectionUtil.connectdb();
    }


    @FXML

    public void handleRetourAction(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        dialogStage = (Stage) source.getScene().getWindow();
        dialogStage.close();
        scene = new Scene(FXMLLoader.load(getClass().getResource("FXMLDocument.fxml")));
        dialogStage.setScene(scene);
        dialogStage.show();
    }

    @FXML
    private void ADD (ActionEvent event) throws IOException {


       {
            String login = textIdentifiant.getText().toString();
            String mail = TextEmail.getText().toString();
            String password = textPassword.getText();

            String sql = "INSERT INTO user_edk (login,mail,password)VALUES(?,?,PASSWORD(?)) ";

            try {
                preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setString(1, login);
                preparedStatement.setString(2, mail);
                preparedStatement.setString(3, password);
                preparedStatement.executeUpdate();


                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Confirm");
                    alert.setHeaderText("Registration successful");
                    alert.setContentText("Your account has been created !");
                    alert.show();
                    Node source = (Node) event.getSource();
                    dialogStage = (Stage) source.getScene().getWindow();
                    dialogStage.close();
                    scene = new Scene(FXMLLoader.load(getClass().getResource("FXMLDocument.fxml")));
                    dialogStage.setScene(scene);
                    dialogStage.show();





            } catch (Exception e) {
                e.printStackTrace();
            }




        }

    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}