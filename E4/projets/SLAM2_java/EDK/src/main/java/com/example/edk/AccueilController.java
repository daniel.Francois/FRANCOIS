package com.example.edk;



import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;


import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 *
 * @author Bushan Sirgur
 */
public class AccueilController implements Initializable {


    Stage dialogStage = new Stage();
    Scene scene;





    Connection connection = null;
    PreparedStatement preparedStatement = null;
    ResultSet resultSet = null;
    private Stage stage;


    @FXML

    public void handleAjoutAction(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        dialogStage = (Stage) source.getScene().getWindow();
        dialogStage.close();
        scene = new Scene(FXMLLoader.load(getClass().getResource("Client.fxml")));
        dialogStage.setScene(scene);
        dialogStage.show();
    }

    public void handleSupprimerAction(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        dialogStage = (Stage) source.getScene().getWindow();
        dialogStage.close();
        scene = new Scene(FXMLLoader.load(getClass().getResource("Clientsupr.fxml")));
        dialogStage.setScene(scene);
        dialogStage.show();
    }


    public void handleVisualiserAction(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        dialogStage = (Stage) source.getScene().getWindow();
        dialogStage.close();
        scene = new Scene(FXMLLoader.load(getClass().getResource("Visualiser.fxml")));
        dialogStage.setScene(scene);
        dialogStage.show();
    }


    public void handleQuitterAction(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        dialogStage = (Stage) source.getScene().getWindow();
        dialogStage.close();
        scene = new Scene(FXMLLoader.load(getClass().getResource("FXMLDocument.fxml")));
        dialogStage.setScene(scene);
        dialogStage.show();
    }


    public void handleModifierAction(ActionEvent event) throws IOException {
        Node source = (Node) event.getSource();
        dialogStage = (Stage) source.getScene().getWindow();
        dialogStage.close();
        scene = new Scene(FXMLLoader.load(getClass().getResource("Visualiser.fxml")));
        dialogStage.setScene(scene);
        dialogStage.show();
    }



    @Override
    public void initialize(URL url, ResourceBundle rb) {



    }
}