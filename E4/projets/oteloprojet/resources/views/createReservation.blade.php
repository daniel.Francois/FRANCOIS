@extends('layouts.app')
@section('content')
<div class="container">
    <form method="post" >
      @csrf 
        <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
        </div>

        <div class="form-group">
            <label for="start">Date début</label>
            <input type="date" class="form-control" name="dateDebut" id="start">
        </div>

        
        <div class="form-group">
            <label for="start">Date fin</label>
            <input type="date" class="form-control" name="dateFin" id="start">
        </div>

        <div class="form-check">
            <input type="checkbox" class="form-check-input" id="exampleCheck1">
            <label class="form-check-label" for="exampleCheck1">Check me out</label>
        </div>
        <div class="form-group">
            <label for="sel1">Periode:</label>
            <select name="idperiode" class="form-control" id="sel1">
                <option value=1>basse</option>
                <option value=2>moyenne</option>
                <option value=3>haute</option>
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
@stop