# FRANCOIS Daniel

22/09/2022 

La Commission européenne veut imposer des règles plus strictes aux fabricants d'objets connectés afin d'améliorer la protection contre les cyberattaques.
Avec le «Cyber Resilience Act», la Commission européenne souhaite renforcer le niveau de sécurité des objets connectés et des produits numériques.

De lourdes sanctions financières

«En introduisant la cybersécurité dès la conception, la législation contribuera à protéger l'économie européenne et notre sécurité collective», a-t-il expliqué. Le projet de règlement sur la cyber résilience doit encore être négocié pendant plusieurs mois par les eurodéputés et les Etats membres.
Les produits et logiciels ne pourront être commercialisés que s'ils respectent des critères de sécurité.

lien: https://www.techniques-ingenieur.fr/actualite/articles/leurope-veut-plus-de-securite-dans-les-objets-connectes-115362/


Source: Google alerts
